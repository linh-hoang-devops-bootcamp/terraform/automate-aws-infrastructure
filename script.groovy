def buildJar() {
    echo 'building the application...'
    sh 'mvn package'
}

def buidImage() {
    echo "building the docker image..."
    withCredentials([usernamePassword(credentialsId: 'docker-hub-repo', passwordVariable: 'PASS', usernameVariable: 'USER')]){
            sh "docker build -t blooncloud/demo-app:jma-3.0 ."
            sh 'echo $PASS | docker login -u $USER --password-stdin'
            sh "docker push blooncloud/demo-app:jma-3.0"
    }
}

def deployApp() {
     echo 'deploying the application'
}
return this